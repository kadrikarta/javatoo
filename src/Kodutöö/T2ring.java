package Kodutöö;

/**
 * Created by Kadri on 17.11.2015.
 */
public class T2ring {

    Integer tulemus;

    /**
     * Ühe täringu veeretamine.
     * @return Ühe täringu veeretamise tulemus.
     */
    public Integer veereta() {

        tulemus = (int)(Math.random() * 6 + 1);

        return tulemus;
    }

}
