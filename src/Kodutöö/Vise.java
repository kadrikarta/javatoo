package Kodutöö;

import java.util.Arrays;

/**
 * Created by Kadri on 18.11.2015.
 */
public class Vise {
    T2ring taring1 = new T2ring();
    T2ring taring2 = new T2ring();
    T2ring taring3 = new T2ring();
    T2ring taring4 = new T2ring();
    T2ring taring5 = new T2ring();

    /**
     * Täringute esmane veeretamine. Visatakse kõik 5 täringut.
     */
    public void viska(){
        viska(true, true, true, true, true);
    }

    /**
     * Soovitud täringute uuesti veeretamine kui boolean on true.
     * @param esimene taring1
     * @param teine tarin2
     * @param kolmas tarin3
     * @param neljas tarin4
     * @param viies taring5
     */
    public void viska(Boolean esimene, Boolean teine, Boolean kolmas, Boolean neljas, Boolean viies){
        if(esimene) taring1.veereta();
        if(teine) taring2.veereta();
        if(kolmas) taring3.veereta();
        if(neljas) taring4.veereta();
        if(viies) taring5.veereta();
    }

    /**
     * Trükib kasutajale välja täringuviske tulemuse.
     * @param i i=0 siis trükib ühes järjekorras, i=1 siis trükib üksteise alla.
     */
    public void prindiTulemus(int i){

        if (i == 0)
            System.out.println(taring1.tulemus + "   " + taring2.tulemus + "   " + taring3.tulemus + "   " +
                    taring4.tulemus + "   " + taring5.tulemus);

        if (i == 1) {
            System.out.println("Täring 1: " + taring1.tulemus);
            System.out.println("Täring 2: " + taring2.tulemus);
            System.out.println("Täring 3: " + taring3.tulemus);
            System.out.println("Täring 4: " + taring4.tulemus);
            System.out.println("Täring 5: " + taring5.tulemus);
        }
    }

    /**
     * Annab viske tulemuse eest punktid.
     * @param nr Mängija valitud kombinatsiooni järjekorranumber.
     * @return Viske punktisumma.
     */
    public int v22rtusta(int nr){

        int[] tulemused = {taring1.tulemus, taring2.tulemus,
                taring3.tulemus, taring4.tulemus, taring5.tulemus};

        Arrays.sort(tulemused);

        switch(nr) {
            case (1): case (2): case (3): case (4):  case (5): case (6): //Ühed kuni kuued
                return numbriKombod(tulemused, nr);
            case (7):
                return paar(tulemused);
            case (8):
                return kaksPaari(tulemused);
            case (9):
                return kolmik(tulemused);
            case (10):
                return nelik(tulemused);
            case (11): case (12):
                return read(tulemused, nr);
            case (13):
                return maja(tulemused);
            case (14): //Summa
                return taring1.tulemus + taring2.tulemus + taring3.tulemus + taring4.tulemus + taring5.tulemus;
            case (15): //Yatzy
                return numbriKombod(tulemused, nr);
            default:
               return 0;
        }

    }

    /**
     * Kontrollib ja teatab, kas mängija viskas esimese korraga Yatzy.
     */
    public void yatzyKontroll() {

        //Kontrollin, kas on Yahtzee.
        if (taring1.tulemus == taring2.tulemus && taring2.tulemus == taring3.tulemus &&
                taring3.tulemus == taring4.tulemus && taring4.tulemus == taring5.tulemus){
            System.out.println("Yatzy!!!");
        }

    }

    /**
     * Kontrollib ja väärtustab ühed kuni kuued ja Yatzy.
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @param nr Väärtustatava kombinatsiooni järjekorranumber (1-6 vastab numbrikombinatsiooni nimele).
     * @return Kombinatsiooni eest antav punktisumma.
     */
    public static int numbriKombod(int[] tulemused, int nr){

        int summa = 0;

        if(nr > 0 && nr < 7){ //numbrid 1 - 6

            for (int i : tulemused)
                if (i == nr){
                    summa += nr;
                }

        }else if(nr == 15){//Yahtzee
            if (tulemused[0] == tulemused[1] && tulemused[1] == tulemused[2] && tulemused[2] == tulemused[3]
                    && tulemused [3] == tulemused[4])
                summa = 50;
        }

        return summa;
    }

    /**
     * Kontrollib ja väärtustab ühe paari.
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @return Kombinatsiooni eest antav punktisumma.
     */
    public static int paar(int[] tulemused){

        int loendus = 0;
        int nr;

        System.out.println("Millise arvu paari tahad skoorida?");
        while(true){
            System.out.println("Sisesta 1 - 6");
            nr = TextIO.getlnInt();
            if (nr > 0 && nr < 7) {
                break;
            }
        }
        for (int i= 0 ; i < tulemused.length; i++){
            if (tulemused[i] == nr){
                loendus += 1;
            }
        }
        if (loendus >= 2) {
            return nr * 2;
        }else {
            return 0;
        }
    }

    /**
     * Kontrollib ja väärtustab 2 paari.
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @return Kombinatsiooni eest antav punktisumma.
     */
    public static int kaksPaari(int[] tulemused){

        System.out.println("I paar.");
        int paar1 = paar(tulemused);
        System.out.println("II paar.");
        int paar2 = paar(tulemused);

        if (paar1 > 0 && paar2 > 0){
            return paar1 + paar2;
        }else{
            return 0;
        }

    }

    /**
     * Kontrollib ja väärtustab kolmiku.
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @return Kombinatsiooni eest antav punktisumma.
     */
    public static int kolmik(int[]tulemused) {

        int summa = 0;

        if (tulemused[0] == tulemused[1] && tulemused[1] == tulemused[2]) {
            summa = tulemused[0] * 3;
        } else if (tulemused[1] == tulemused[2] && tulemused[2] == tulemused[3]) {
            summa = tulemused[1] * 3;
        } else if (tulemused[2] == tulemused[3] && tulemused[3] == tulemused[4]) {
            summa = tulemused[2] * 3;
        }

        return summa;
    }

    /**
     * Kontrollib ja väärtustab neliku.
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @return Kombinatsiooni eest antav punktisumma.
     */
    public static int nelik(int[] tulemused) {

        int summa = 0;

        if (tulemused[0] == tulemused[1] && tulemused[1] == tulemused[2] &&
                tulemused[2] == tulemused[3]) {
            summa = tulemused[0] * 4;
        } else if (tulemused[1] == tulemused[2] && tulemused[2] == tulemused[3] &&
                tulemused[3] == tulemused[4]) {
            summa = tulemused[1] * 4;
        }

        return summa;

    }

    /**
     * Kontrollib ja väärtustab väikese ja suure rea.
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @return Kombinatsiooni eest antav punktisumma.
     */
    public static int read(int[]tulemused, int nr) {

        int summa = 0;

        if (nr == 11 || nr == 12) {
            if (tulemused[0] == 1 || tulemused[0] == 2) {
                if (tulemused[0] + 1 == tulemused[1] && tulemused[1] + 1 == tulemused[2] &&
                        tulemused[2] + 1 == tulemused[3] && tulemused[3] + 1 == tulemused[4]) {
                    if (tulemused[0] == 1) {
                        summa = 15;
                    } else {
                        summa = 20;
                    }

                } else {
                    summa = 0;
                }
            }
        }

        return summa;
    }

    /**
     * Kontrollib ja väärtustab maja
     * @param tulemused Viske tulemustest moodustatud ja sorteeritud massiv.
     * @return Kombinatsiooni eest antav punktisumma.
     */
     public static int maja(int[] tulemused) {

         int summa = 0;

         if (tulemused[0] == tulemused[1]) {
             if (tulemused[1] == tulemused[2]) {
                 if (tulemused[3] == tulemused[4]) {
                     summa = tulemused[0] * 3 + tulemused[3] * 2;
                 }
             } else if (tulemused[2] == tulemused[3] && tulemused[3] == tulemused[4]) {
                 summa = tulemused[0] * 2 + tulemused[2] * 3;
             }

         }

         return summa;
     }

}

